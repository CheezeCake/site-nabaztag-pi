#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = u'Alonso Jimenez Julien, Nicolet Emmanuel'
SITENAME = u'Nabaztag Pi'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'

DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False

MENUITEMS = (
            ('Home', 'index.html'),
            ('Comptes rendus', 'comptes_rendus.html'),
            ('Avancement', 'avancement.html'))

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

# Blogroll
LINKS = (('Nabaztag', 'http://fr.wikipedia.org/wiki/Nabaztag'),
         ('Raspberry Pi', 'http://www.raspberrypi.org'),)

# Social widget
# SOCIAL = (('You can add links in your config file', '#'),
#           ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# THEME = os.environ['HOME'] + "/pelican-themes/subtle"
THEME = "../subtle"
